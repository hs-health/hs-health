# Design Document

This document details the inners of this Hidden Service Health tool (HSH -
copyleft pending :).

## Goals

We analyze five hidden services that we consider stable. They are not run nor
affiliated with Tor project.

* wlupld3ptjvsgwqw
* agorahooawayyfoe
* 4cjw6cwpeaeppfqz
* facebookcorewwwi
* 3g2upl4pq6kufc4m

The goal is to test HS reachability thus the overall health of the hidden
service network.

## Technical Analysis

Every hour, there is a new consensus voted by the directory authorities. It
ends up on CollecTor[1] around 5 to 8 minutes after the hour. We fetch it at
that point, parse it and run the analysis detailed below.

For each onion address, we run the exact same analysis so for the rest of the
document, we'll refer to a single hidden service (HS).

### New Consensus

We schedule an analysis for an onion address at each hour plus 8 minutes (when
available on on CollecTor). Once we have the consensus, we compute the current
descriptor IDs (both replica) and build the list of responsible hidden service
directories (HSDir).

Assume `d`, the current descriptor ID of an HS and `hsdirs` the current
responsible directories. Also, `d'` is the previous descriptor ID that is from
the previous hour and `hsdirs'` the previous directories.

+ If `d == d'`, meaning no new descriptor ID, we check for churn i.e if an
HSDir in `hsdirs'` is not present in `hsdirs`, the current list.

	- If `diff(hsdirs, hsdirs') > 0`, we have a churn effect. This means we end
	up with a list of HSDir that might **not** have the descriptor with ID `d`.
	We then launch a fetch on the new HSDir(s).

		+ On failure, we reshedule a fetch 15 minutes later because every 15
		minutes, the HS re-uploads a descriptor. Also, chances are that the
		HS doesn't have the consensus we are looking at so we are going to
		retry until we succeed or give up after 3 hours.

		+ On success, we log it. It might be because we retried multiple times
		so we log the delay between the first fail and this success. This
		tells us how much time the HSDir couldn't provide a descriptor for
		the HS.

	We also launch a fetch on the HSDir(s) that have been kicked out. First we
	check if they are alive meaning in the current consensus and if so we
	launch the fetch for `d` and log the result.

	- If there is no churn, we are going to do a fetch on all HSDir in `hsdirs`
	and log the result.

+ If `d != d'`, we have a descriptor ID rotation thus a complete new set of
HSDirs. We launch a fetch on each HSDir in `hsdirs` for `d` and the same for
`hsdirs'` for `d'`

#### Fetch events

For each fetch on a HSDir, control events are triggered depending on the result
of the fetch. Below are the details of what happens for each of them.

- `REQUESTED`: Log an initiated fetch counter. We then schedule a timeout job
  which is trigger after 120 seconds of not receiving a success or failed
  event. If this timeout event is triggered, it's logged in a timout fetch
  counter and we reschedule a fetch in 15 minutes to see if this is a recurrent
  issue and will stop after 3 hours of timeout.

- `RECEIVED`: Log success fetch counter.

- `FAILED`: Log failure fetch counter.

	+ We reschedule a fetch in 15 minutes. It could be that the HSDir doesn't
	have the descriptor because of a churn or restart but also could be
	malicious or a bug in Tor code.

	+ After 3 hours of retrying, we stop the attempt and log the issue.

[1] https://collector.torproject.org/recent/relay-descriptors/consensuses/
