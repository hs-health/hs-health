#!/usr/bin/env python

# Analyze the health.csv file generated by health.py. Use --verbose for extra
# information on each onion address.

import csv
import datetime
import os
import sys

from time import strftime, gmtime

verbose = False

def print_results(onions, nr_hours):
    for addr in onions:
        total_time = fail_count = 0
        onion = onions[addr]
        data = onion['data']
        fetch_count = onion['fetch_count']
        fail_len = len(data)

        if fail_len == 0:
            print("[+] %s.onion\n    Quiescent state." % (addr))
            return

        fail_same_ts = 0
        for elem in data:
            fail_count += elem['count']
            total_time += elem['elapsed']
            if fail_same_ts == 0:
                fail_same_ts = elem['ts_first_fail']
            else:
                if fail_same_ts + (60 * 60) >= elem['ts_first_fail']:
                    print("Double fail")

        fail_prob = float(fail_count / fetch_count) * 100.0
        time_sec = float(total_time / fail_len)
        avg_time = strftime('%H:%M:%S', gmtime(time_sec))

        print("[+] %s.onion" % (addr))
        print("    %.2f%% of failed fetch (%d/%d) for an average time of %s minutes (%d seconds)" %
                (fail_prob, fail_count, fetch_count, avg_time, time_sec))
        print("    After first fail on an HSDir, we have %.2f failed attempt(s) before success" %
                (float(fail_count / fail_len)))
        print("    Churn happened %.3f%% of the time (%d times)" % ((float(fail_len / nr_hours) * 100), fail_len))
        print("")

        if len(sys.argv) > 1 and sys.argv[1] == '--verbose':
            for elem in data:
                time_sec = elem['elapsed']
                time_min = strftime('%H:%M:%S', gmtime(time_sec))
                start_date = strftime('%d %b %Y %H:%M:%S', gmtime(elem['ts_first_fail']))
                end_date = strftime('%H:%M:%S', gmtime(elem['ts_first_success']))

                print("    HSDir %s didn't have %s:" %
                        (elem['hsdir'], elem['desc_id']))
                print("    --> From %s to %s - %s delay (%d seconds) with %d failed fetch." %
                        (start_date, end_date, time_min, time_sec, elem['count']))


def print_summary(onions):
    """ Print analysi summary. """

    total_time = total_count = fail_count = 0
    total_data_len = 0

    for addr in onions:
        onion = onions[addr]
        data = onion['data']
        total_count += onion['fetch_count']
        fail_len = len(data)
        if fail_len == 0:
            continue
        total_data_len += fail_len

        # Count fail event.
        for elem in data:
            fail_count += float(elem['count'])
            total_count += float(elem['count'])
            total_time += elem['elapsed']

    time_avg = float(total_time / total_data_len)
    fail_prob = float(fail_count / total_count) * 100.0

    print("--> %.3f%% failed fetch (%d/%d)." %
          (fail_prob, fail_count, total_count))
    print("    On average once we fail to fetch once on a specific HSDir, the descriptor was missing for %s (%d seconds)." %
          (strftime('%H:%M:%S', gmtime(time_avg)), time_avg))
    print("")

def run(filename):
    fields_name = ['desc_id', 'hsdir_fp', 'onion_addr', 'ts_first_used_descid',
            'ts_hsdir_start_resp', 'ts_first_fail', 'ts_last_fail',
            'total_fail', 'ts_first_success', 'last_fail_reason']

    fd = open(filename, 'r')
    d = csv.DictReader(fd, delimiter=',', fieldnames=fields_name)
    onions = {}
    # Keep the log period.
    first_ts = last_ts = None

    for item in d:
        last_ts = float(item['ts_first_success'])
        if first_ts is None:
            first_ts = last_ts

        onion_addr = item['onion_addr']
        if onion_addr not in onions:
            onions[onion_addr] = {
                    'fetch_count': 0,
                    'data': []
                }
        onion = onions[onion_addr]
        onion['fetch_count'] += 1

        first_fail = item['ts_first_fail']
        if first_fail == 'na':
            continue

        first_fail = float(first_fail)
        num_fail = float(item['total_fail'])
        first_success = float(item['ts_first_success'])

        onion['data'].append({
                'count': num_fail,
                'ts_first_fail': first_fail,
                'ts_first_success': first_success,
                'elapsed': first_success - first_fail,
                'hsdir': item['hsdir_fp'],
                'desc_id': item['desc_id']
            })

    nr_hours = (((last_ts - first_ts) / 60) / 60)
    print("Log health.csv period is from %s to %s (%d hours)" %
            (strftime('%d %b %Y %H:%M:%S', gmtime(first_ts)),
             strftime('%d %b %Y %H:%M:%S', gmtime(last_ts)),
             nr_hours))

    print_summary(onions)

    #if verbose is True:
    print_results(onions, nr_hours)
    fd.close()

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == '--verbose':
        verbose = True
    filename = "health.csv"
    if not os.path.isfile(filename):
        print("%s not found" % (filename))
        sys.exit(1)

    run(filename)
