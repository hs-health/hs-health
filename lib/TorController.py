# -*- coding: utf-8 -*
#
# Copyright (C) 2015 - David Goulet <dgoulet@ev0ke.net>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; only version 2 of the License.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA  02111-1307, USA.

import sys
import stem

from stem import SocketError
from stem.control import Controller, EventType

class TorController():
    """
    DOCDOC
    """

    def __init__(self, context, addr=None, port=None):
        self.addr = "127.0.0.1"
        self.port = 9051
        self.context = context

        if addr is not None:
            self.address = addr
        if port is not None:
            self.port = port

        try:
            self.controller = Controller.from_port(self.addr, self.port)
        except stem.SocketError as err:
            print("Unable to connect to Tor control port")
            sys.exit(1)

        try:
            self.controller.authenticate()
        except stem.connection.AuthenticationFailure as err:
            print("Unable to authenticate to Tor control port")
            sys.exit(1)

        # We always want the latest information.
        self.controller.set_caching(False)

    def _build_hsfetch_cmd(self, onion_addr, desc_id, hsdirs):
        # Build HSFETCH command.
        cmd = "HSFETCH"
        if onion_addr is not None:
            cmd += " %s" % (onion_addr)
        elif desc_id is not None:
            cmd += " v2-%s" % (desc_id)

        if hsdirs is not None:
            for hsdir in hsdirs:
                cmd += " SERVER=%s" % (hsdir)
        return cmd

    def close(self):
        self.controller.close()

    def add_event(self, callback, ev_type):
        self.controller.add_event_listener(callback, ev_type)

    def del_event(self, callback):
        self.controller.remove_event_listener(callback)

    def get_consensus(self):
        return self.controller.get_network_statuses()

    def fetch_hs_descriptor(self, log_addr, onion_addr=None, desc_id=None, hsdirs=None):
        # First send NEWNYM to flush the cache.
        #self.controller.signal(stem.control.Signal.NEWNYM)

        cmd = self._build_hsfetch_cmd(onion_addr, desc_id.lower(), hsdirs)
        if desc_id is not None and (hsdirs is None or len(hsdirs) == 0):
            # Skip the command because it will fail but still log it.
            pass
        else:
            reply = self.controller.msg(cmd)
            if not reply.is_ok():
                print("HSFETCH %s failed" % (cmd))

        self.context.log.info("[%s] %s" % (log_addr, cmd))
